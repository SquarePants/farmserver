const express = require('express');
const forestController = require('../controllers/forest');

const router = express.Router();

router.get('/:farmer', forestController.getForest);
router.get('/plant/:forest', forestController.plantTree);
router.get('/collect/:forest', forestController.collectWood);
router.get('/build/:forest', forestController.buildHouse);

module.exports = router;
