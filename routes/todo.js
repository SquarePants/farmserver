const express = require('express');

const router = express.Router();
const todoController = require('../controllers/todo');
// CRUD
router.get('/', todoController.fetchTodoList);
router.post('/', todoController.saveToDo);
router.get('/:id', todoController.getTodoById);

module.exports = router;
