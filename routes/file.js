const express = require('express');
const fileController = require('../controllers/file');

const router = express.Router();

router.get('/image/:id', fileController.downloadImage);

module.exports = router;
