const express = require('express');
const upload = require('../services/file');

const router = express.Router();
const productController = require('../controllers/product');

router.post('/', upload.array('images'), productController.addProduct);
router.get('/', productController.fetchProducts);
module.exports = router;
