// const productRoutes = require('./product');
const userRoutes = require('./user');
const fileRoutes = require('./file');
const forestRoutes = require('./forest');

module.exports = (app) => {
  // app.use('/api/product', productRoutes);
  app.use('/api/user', userRoutes);
  app.use('/api/file', fileRoutes);
  app.use('/api/forest', forestRoutes);
};
