require('dotenv').config();
const express = require('express');
const http = require('http');
const socketio = require('socket.io');
const routes = require('./routes/index');
const middlewares = require('./config/middlewares');
const socketEvents = require('./services/socket');
const database = require('./config/database');
const jobs = require('./services/cycle');

const app = express();

database.connect();

middlewares(app);
routes(app);

app.use((err, req, res, next) => {
  console.log(err);
  res.status(404).json(err);
});
const server = http.createServer(app).listen(process.env.PORT, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log(`Server running on : ${process.env.PORT}`);
  }
});
const io = socketio(server);
socketEvents(io);
