const mongoose = require('mongoose');

const TodoSchema = mongoose.Schema({
  title: { type: String },
  content: { type: String },
  image: { type: mongoose.Schema.Types.ObjectId, ref: 'Image' },
});
module.exports = mongoose.model('Todo', TodoSchema);
