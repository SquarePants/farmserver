const mongoose = require('mongoose');
const constants = require('../services/constants');

const UserSchema = mongoose.Schema(
  {
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    role: {
      type: String,
      enum: [constants.USER_ROLES.CLIENT, constants.USER_ROLES.FARMER],
      default: constants.USER_ROLES.CLIENT,
    },
    hash: { type: String },
    salt: { type: String },
  },
  { discriminatorKey: 'role' }
);

const ClientSchema = mongoose.Schema({
  location: {
    country: { type: String },
    city: { type: String },
    address: { type: String },
  },
  preferredProducts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Product' }],
});
const FarmerSchema = mongoose.Schema({
  location: {
    country: { type: String },
    city: { type: String },
    address: { type: String },
  },
  climate: { type: String },
  products: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Product' }],
  forest: { type: mongoose.Schema.Types.ObjectId, ref: 'Forest' },
});

const User = mongoose.model('User', UserSchema);

module.exports = {
  User,
  Client: User.discriminator(constants.USER_ROLES.CLIENT, ClientSchema),
  Farmer: User.discriminator(constants.USER_ROLES.FARMER, FarmerSchema),
};
