const mongoose = require('mongoose');
const constants = require('../services/constants');

const ProductSchema = mongoose.Schema({
  name: { type: String, required: true },
  variety: { type: String, required: true },
  description: { type: String },
  type: {
    type: String,
    enum: [constants.PRODUCT_TYPES.FRUIT, constants.PRODUCT_TYPES.VEGETABLE],
    required: true,
  },
  quantity: { type: Number, default: 0, min: 0 },
  images: [{ type: mongoose.Schema.Types.ObjectId, ref: 'File' }],
});

module.exports = mongoose.model('Product', ProductSchema);
