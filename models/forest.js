const mongoose = require('mongoose');

const forestSchema = mongoose.Schema({
  forestName: { type: String, required: true },
  treeCount: { type: Number, default: 0, min: 0 },
  woodCount: { type: Number, default: 0, min: 0 },
  houseCount: { type: Number, default: 0, min: 0 },
  farmerCount: { type: Number, default: 0, min: 0 },
  farmer: { type: mongoose.Schema.Types.ObjectId, ref: 'Farmer' },
});
/* forestSchema.post('updateOne', async (doc) => {
  switch (doc.workerCount) {
    case 0:
      if (doc.woodCount > 998) {
        await forestSchema.updateOne({ _id: doc._id }, { $inc: { workerCount: 1 } });
      }
      break;
    case 1:
      if (doc.woodCount > 994) {
        await forestSchema.updateOne({ _id: doc._id }, { $inc: { workerCount: 1 } });
      }
      break;
    default:
  }
}); */
module.exports = mongoose.model('Forest', forestSchema);
