const mongoose = require('mongoose');

const FileSchema = mongoose.Schema({
  filename: {
    type: String,
    required: true,
  },
  mimetype: {
    type: String,
  },
  destination: { type: String },
  size: { type: Number },
});

module.exports = mongoose.model('File', FileSchema);
