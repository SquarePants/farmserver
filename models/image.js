const mongoose = require('mongoose');

const ImageSchema = mongoose.Schema({
  filename: {
    type: String,
    required: true,
  },
  mimetype: {
    type: String,
  },
  destination: { type: String },
  size: { type: Number },
});

module.exports = mongoose.model('Image', ImageSchema);
