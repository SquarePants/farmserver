/* eslint-disable no-await-in-loop */
const ProductSchema = require('../models/product');
const FileSchema = require('../models/file');

exports.addProduct = async (req, res, next) => {
  try {
    const images = [];
    for (const file of req.files) {
      const image = await new FileSchema(file).save();
      images.push(image);
    }
    const newProduct = await new ProductSchema({ ...req.body, images }).save();
    res.status(200).json(newProduct);
  } catch (error) {
    next(error);
  }
};

exports.fetchProducts = async (req, res, next) => {
  try {
    const productList = await ProductSchema.find({}).lean();
    if (!productList || productList.length < 1) {
      next('no products found');
    }
    res.status(200).json(productList);
  } catch (error) {
    next(error);
  }
};
