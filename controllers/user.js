const { genPassword, issueJWT, validPassword } = require('../services/jwt');
const { User: UserSchema } = require('../models/user');

exports.register = async (req, res, next) => {
  try {
    console.log(req.body);
    const saltHash = genPassword(req.body.password);
    const { salt, hash } = saltHash;
    const newUser = await new UserSchema({
      name: req.body.name,
      email: req.body.email,
      role: req.body.role,
      hash,
      salt,
    }).save();
    res.status(200).json({ success: true, role: newUser.role });
  } catch (error) {
    next(error);
  }
};
exports.login = async (req, res, next) => {
  try {
    console.log('hello world');
    console.log(req.body);
    const user = await UserSchema.findOne({ email: req.body.email }).lean();
    if (!user || !user.hash || !user.salt) {
      console.log('User not found or invalid credentials');
      next('User not found or invalid credentials');
    }
    if (!validPassword(req.body.password, user.hash, user.salt)) {
      console.log('User not found or invalid credentials');
      next('User not found or invalid credentials');
    }
    res.status(200).json({ success: true, token: issueJWT(user) });
  } catch (error) {
    console.log(error);
    next(error);
  }
};
