const TodoSchema = require('../models/todo');
const ImageSchema = require('../models/image');

exports.getTodoById = async (req, res, next) => {
  try {
    const todoId = req.params.id;
    const todo = await TodoSchema.findById(todoId).lean();
    if (!todo) {
      next("couldn't find todo");
    }
    res.status(200).json(todo);
  } catch (error) {
    next(error);
  }
};
exports.saveToDo = async (req, res, next) => {
  try {
    const image = await new ImageSchema(req.file).save();
    const todo = await new TodoSchema({ ...req.body, image: image._id }).save();
    if (!todo) {
      next('error while creating todo');
    }
    res.status(200).json(todo);
  } catch (error) {
    next(error);
  }
};
exports.fetchTodoList = async (req, res, next) => {
  try {
    const todoList = await TodoSchema.find({}).populate('image', 'filename -_id').select().lean();
    if (todoList.length < 1) {
      next('No Todo available');
    }
    res.status(200).json(todoList);
  } catch (error) {
    next(error);
  }
};
