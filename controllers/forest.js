const ForestSchema = require('../models/forest');
const { Farmer: FarmerSchema } = require('../models/user');

const initForest = async (farmer) => {
  try {
    const newForest = await new ForestSchema({ forestName: `Forest${farmer}`, farmer }).save();
    await FarmerSchema.findOneAndUpdate({ _id: farmer }, { forest: newForest._id });
    return newForest;
  } catch (error) {
    return null;
  }
};

exports.getForest = async (req, res, next) => {
  try {
    let forest;
    forest = await ForestSchema.findOne({ farmer: req.params.farmer }).lean();
    if (!forest) {
      forest = await initForest(req.params.farmer);
      if (!forest) {
        next('no forest');
      }
    }
    res.status(200).json({ success: true, forest });
  } catch (error) {
    next(error);
  }
};
exports.plantTree = async (req, res, next) => {
  try {
    await ForestSchema.updateOne({ _id: req.params.forest }, { $inc: { treeCount: 1 } });
    res.status(200).json({ success: true });
  } catch (error) {
    next(error);
  }
};
exports.collectWood = async (req, res, next) => {
  try {
    await ForestSchema.updateOne({ _id: req.params.forest }, { $inc: { woodCount: 5, treeCount: -1 } });
    res.status(200).json({ success: true });
  } catch (error) {
    next(error);
  }
};
exports.buildHouse = async (req, res, next) => {
  try {
    await ForestSchema.updateOne({ _id: req.params.forest }, { $inc: { houseCount: 1, woodCount: -1000 } });
    res.status(200).json({ success: true });
  } catch (error) {
    next(error);
  }
};
