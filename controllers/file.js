const FileSchema = require('../models/file');

exports.downloadImage = async (req, res, next) => {
  try {
    const image = await FileSchema.findById(req.params.id).lean();
    if (!image) {
      next("couldn't find image");
    }
    res.status(200).download(`${image.destination}/${image.filename}`);
  } catch (ex) {
    next(ex);
  }
};
