const { ExtractJwt, Strategy: JwtStrategy } = require('passport-jwt');
const { User } = require('../models/user');

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET,
  algorithms: ['RS256'],
};
const strategy = new JwtStrategy(options, (payload, done) => {
  User.findOne({ _id: payload.id })
    .then((user) => {
      if (user) return done(null, user);
      return done(null, false);
    })
    .catch((err) => done(err));
});

module.exports = (passport) => {
  passport.use(strategy);
};
