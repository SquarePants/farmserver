module.exports = {
  USER_ROLES: {
    CLIENT: 'Client',
    FARMER: 'Farmer',
  },
  PRODUCT_TYPES: {
    FRUIT: 'FRUIT',
    VEGETABLE: 'VEGETABLE',
  },
  EVENTS: [
    {
      event: 'FOREST_FIRE',
      chance: 0.9,
      effect: { attribute: 'treeCount', value: 0.9 },
    },
    {
      event: 'FLOODING',
      chance: 0.4,
      effect: { attribute: 'houseCount', value: 0.8 },
    },
    {
      event: 'WET_WOOD',
      chance: 0.9,
      effect: { attribute: 'woodCount', value: 0.9 },
    },
  ],
};
