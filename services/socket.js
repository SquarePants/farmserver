const moment = require('moment');
const { Farmer } = require('../models/user');

const Timeleft = () => 60 - moment().seconds();
module.exports = (io) => {
  io.on('connection', (socket) => {
    socket.on('auth::request', async ({ farmerId }) => {
      const exists = await Farmer.countDocuments({ _id: farmerId });
      if (!exists) {
        socket.emit('auth::nope', { message: 'Auth failed' });
      } else {
        socket.emit('auth::ok');
        socket.emit('progress::update', { timeleft: Timeleft() });
      }
    });
    socket.on('progress::renew', () => {
      socket.emit('progress::update', { timeleft: Timeleft() });
    });
  });
};
