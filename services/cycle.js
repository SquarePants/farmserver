/* eslint-disable no-restricted-syntax */
const schedule = require('node-schedule');
const { shuffle } = require('lodash');
const ForestSchema = require('../models/forest');
const { EVENTS } = require('./constants');

const populateEmptyHouses = (forest) => {
  if (forest.farmerCount < forest.houseCount) {
    const random = Math.random();
    if (random <= 0.3) {
      console.log(`adding farmer to forest ${forest.forestName}`);
      forest.farmerCount += 1;
      forest.save();
    }
  }
};

const randomEvents = (forest) => {
  const random = 0;
  const randomEvent = shuffle(EVENTS)[0];
  if (randomEvent.chance >= random) {
    console.log(`ran ${randomEvent.event} on ${forest.forestName}`);
    console.log(`{_id: ${forest._id}},{$multiply: { ${[randomEvent.effect.attribute]}: ${randomEvent.effect.value} }}`);
    ForestSchema.updateOne(
      { _id: forest._id },
      { [randomEvent.effect.attribute]: { $multiply: { [randomEvent.effect.attribute]: randomEvent.effect.value } } }
    );
  }
};

module.exports = schedule.scheduleJob('cycle::updateAll', '0 * * * * *', async () => {
  ForestSchema.find()
    .then((forests) => {
      for (const forest of forests) {
        console.log('running the odds');
        populateEmptyHouses(forest);
        randomEvents(forest);
      }
    })
    .catch((err) => console.log(err));
});
